extends CanvasLayer

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

const MAIN_SCENE = "res://stages/stage0.scn"

var is_changing = false

signal stage_changed


func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass
func change_stage(stage_path):
	if is_changing: return
	
	is_changing = true
	get_tree().get_root().set_disable_input(true)
	#fade black
	get_node("anim").play("fade_in")
	yield(get_node("anim"),"finished")
	#change to stage
	get_tree().change_scene(stage_path)
	emit_signal("stage_changed")
	#fade from black
	get_node("anim").play("fade_out")
	yield(get_node("anim"),"finished")
	
	is_changing=false
	get_tree().get_root().set_disable_input(false)
	pass