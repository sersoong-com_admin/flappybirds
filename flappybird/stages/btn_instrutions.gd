extends TextureButton

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	connect("pressed",self,"_on_pressed")
	grab_focus()
	pass
func _on_pressed():
	var bird = utils.Get_main_node().get_node("Bird")
	if bird:
		bird.set_state(bird.STATE_FLAP)
	hide()
	pass