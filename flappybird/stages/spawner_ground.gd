extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

const scn_ground=preload("res://stages/Ground.tscn")
const GROUND_WITDH=168
const Amount_to_fill=2

func _ready():
	for i in range(Amount_to_fill):
		spawn_and_move()
	
	# Called every time the node is added to the scene.
	# Initialization here
	pass
func spawn_and_move():
	spawn_ground()
	go_nextpos()
	
func spawn_ground():
	var new_ground = scn_ground.instance()
	new_ground.set_pos(get_pos())
	new_ground.connect("exit_tree",self,"spawn_and_move")
	get_node("container").add_child(new_ground)
	pass
	
func go_nextpos():
	set_pos(get_pos()+Vector2(GROUND_WITDH,0))
	pass