extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

const scn_pipe = preload("res://stages/pipe.tscn")
const GROUND_HEIGHT = 56
const PIPEWIDTH = 26
const OFFSET_X = 65
const OFFSET_Y=55
const AMOUNT_TO_FILL = 3

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	var bird = utils.Get_main_node().get_node("Bird")
	if bird:
		bird.connect("state_changed",self,"_on_bird_state_changed",[],CONNECT_ONESHOT)
	pass
	
func _on_bird_state_changed(bird):
	if bird.get_state() == bird.STATE_FLAP:
		start()
	pass
func start():
	go_init_pos()
	for i in range(AMOUNT_TO_FILL):
		spawn_and_move()
	pass
func spawn_and_move():
	spawn_pipe()
	go_nextpos()
	pass
func go_init_pos():
	randomize()
	
	var init_pos = Vector2()
	init_pos.x = get_viewport_rect().size.width + PIPEWIDTH/2
	init_pos.y = rand_range(0+OFFSET_Y,get_viewport_rect().size.height-GROUND_HEIGHT-OFFSET_Y)
	var camera = utils.Get_main_node().get_node("camera")
	if camera:
		init_pos.x += camera.get_total_pos().x
	
	set_pos(init_pos)
	
	pass
func spawn_pipe():
	var new_pipe  = scn_pipe.instance()
	new_pipe.set_pos(get_pos())
	new_pipe.connect("exit_tree",self,"spawn_and_move")
	get_node("Container").add_child(new_pipe)
	pass
	
func go_nextpos():
	randomize()
	
	var init_pos = get_pos()
	init_pos.x += PIPEWIDTH/2 + OFFSET_X + PIPEWIDTH/2
	init_pos.y = rand_range(0+OFFSET_Y,get_viewport_rect().size.height-GROUND_HEIGHT-OFFSET_Y)
	set_pos(init_pos)
	
	pass