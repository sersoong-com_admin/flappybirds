extends RigidBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

onready var state = FlyingState.new(self)
var speed = 50
signal state_changed

##bird state
const STATE_FLYING   =0
const STATE_FLAP     =1
const STATE_HIT      =2
const STATE_GROUNDED =3


func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	
	connect("body_enter",self,"_on_body_enter")
	add_to_group(game.GROUP_BIRDS)
	set_fixed_process(true)
	set_process_input(true)
	
	
	
	pass

func _on_body_enter(other_body):
	if state.has_method("on_body_enter"):
		state.on_body_enter(other_body)
	pass

func _fixed_process(delta):
	state.update(delta)
	pass
	
func _input(event):
	state.input(event)
	pass

func set_state(new_state):
	state.exit()
	
	if new_state==STATE_FLYING:
		state=FlyingState.new(self)
	elif new_state==STATE_FLAP:
		state=FlapState.new(self)
	elif new_state==STATE_HIT:
		state=HitState.new(self)
	elif new_state==STATE_GROUNDED:
		state=GroundedState.new(self)
		
	emit_signal("state_changed",self)
	pass
func get_state():
	if state extends FlyingState:
		return STATE_FLYING
	elif state extends FlapState:
		return STATE_FLAP
	elif state extends HitState:
		return STATE_HIT
	elif state extends GroundedState:
		return STATE_GROUNDED
	
	
	pass
	
	
#####class fly state
class FlyingState:
	var bird
	var prev_gravity_scale
	
	func _init(bird):
		self.bird = bird
		bird.get_node("anim").play("flying")
		bird.set_linear_velocity(Vector2(bird.speed,bird.get_linear_velocity().y))
		prev_gravity_scale = bird.get_gravity_scale()
		bird.set_gravity_scale(0)
		
		pass
	func update(delta):
		pass
	func input(event):
		pass
	func exit():
		bird.set_gravity_scale(prev_gravity_scale)
		bird.get_node("anim").stop()
		bird.get_node("anim_sprite").set_pos(Vector2(0,0))
		pass
#####class flap state
class FlapState:
	var bird
	func _init(bird):
		self.bird = bird
		bird.set_linear_velocity(Vector2(bird.speed,bird.get_linear_velocity().y))
		flap()
		pass
	func update(delta):
		if rad2deg(bird.get_rot())>30 :
			bird.set_rot(deg2rad(30))
		if bird.get_linear_velocity().y>0:
			bird.set_angular_velocity(1.5)
		pass
	func input(event):
		if event.is_action_pressed("flap"):
			flap()
		pass
		
	func on_body_enter(other_body):
		if other_body.is_in_group(game.GROUP_PIPES):
			bird.set_state(bird.STATE_HIT)
		elif other_body.is_in_group(game.GROUP_GROUNDS):
			bird.set_state(bird.STATE_GROUNDED)
		pass
	func flap():
		bird.set_linear_velocity(Vector2(bird.get_linear_velocity().x,-150));
		bird.set_angular_velocity(-3)
		bird.get_node("anim").play("flap")
		pass
	func exit():
		pass
#####class hit state
class HitState:
	var bird
	func _init(bird):
		self.bird = bird
		bird.set_linear_velocity(Vector2(0,0))
		bird.set_angular_velocity(2)
		var other_body = bird.get_colliding_bodies()[0]
		bird.add_collision_exception_with(other_body)
		pass
	func update(delta):
		pass
	func input(event):
		pass
	func on_body_enter(other_body):
		if other_body.is_in_group(game.GROUP_GROUNDS):
			bird.set_state(STATE_GROUNDED)
		pass
	func exit():
		pass
#####class ground state
class GroundedState:
	var bird
	func _init(bird):
		self.bird = bird
		bird.set_linear_velocity(Vector2(0,0))
		bird.set_angular_velocity(0)
		bird.set_sleeping(true)
		pass
	func update(delta):
		pass
	func input(event):
		pass
	func exit():
		pass