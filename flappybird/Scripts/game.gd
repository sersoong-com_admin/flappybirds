extends Node

const GROUP_PIPES = "pipes"
const GROUP_GROUNDS = "grounds"
const GROUP_BIRDS = "birds"

const MEDAL_BRONZE = 10
const MEDAL_SILVER =20
const MEDAL_GOLD =30
const MEDAL_PLATINUM = 40

var best_score = 0 setget _set_best_score
var current_score=0 setget _set_current_score

signal score_current_changed
signal score_best_changed

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here

	scenes_manager.connect("stage_changed",self,"_on_stage_changed")
	
	pass
func _on_stage_changed():
	current_score = 0
	pass
func _set_best_score(score):
	if score > best_score:
		best_score = score
		emit_signal("score_best_changed")
	pass
	
func _set_current_score(score):
	current_score = score
	emit_signal("score_current_changed")
	pass